import { Component } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;

  constructor(private afStorage: AngularFireStorage) { }

  uploadFile(event) {
    // this.afStorage.upload('/upload/to/this-path', event.target.files[0]);

    const file = event.target.files[0];
    const filePath = 'name-your-file-path-here';
    const task = this.afStorage.upload(filePath, file);

    this.uploadPercent = task.percentageChanges();
    this.downloadURL = task.downloadURL();
  }


}
